package gr.commonslab.sunnyclist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.TextureView
import android.view.View
import android.widget.TextView
import java.io.File
import androidx.core.app.NavUtils

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //TODO: pass and get values, total distance, consumption, production, time, passengers, month, hour, hours b4, pedal production
        //Show last route's data from log file open routelog.txt
        val infoText = findViewById<TextView>(R.id.infotext)
        //Do not append
        val logFile = File(applicationContext.filesDir, "routelog.txt").readText()
        Thread({
            while (true) {
                this.runOnUiThread {
                    infoText.text = logFile
                }
                Thread.sleep(2000)
            }
        }, "InfoActivity.RefreshValues").start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else              -> super.onOptionsItemSelected(item)
        }
    }
}