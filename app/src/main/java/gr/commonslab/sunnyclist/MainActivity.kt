package gr.commonslab.sunnyclist

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.PointF
import android.os.Bundle
import android.os.Environment
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.here.android.mpa.common.*
import com.here.android.mpa.common.GeoPosition
import com.here.android.mpa.common.MapSettings.setDiskCacheRootPath
import com.here.android.mpa.common.PositioningManager.*
import com.here.android.mpa.guidance.NavigationManager
import com.here.android.mpa.guidance.NavigationManager.MapUpdateMode
import com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener
import com.here.android.mpa.mapping.*
import com.here.android.mpa.mapping.Map
import com.here.android.mpa.routing.*
import com.here.android.mpa.routing.ConsumptionParameters.ConsumptionForSpeed
import com.here.android.mpa.search.*
import com.here.odnp.util.Log
import gr.commonslab.sunnyclist.Globals.routeConsumptions
import gr.commonslab.sunnyclist.R.id.*
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.*
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.List
import kotlin.collections.MutableList
import kotlin.collections.count
import kotlin.collections.indices
import kotlin.collections.isNotEmpty
import kotlin.collections.plus
import kotlin.collections.toTypedArray
import kotlin.math.*
import android.text.Editable
import android.view.Menu
import android.view.MenuItem
import gr.commonslab.sunnyclist.Globals.routeDistances
import java.nio.charset.Charset


public open class MainActivity : AppCompatActivity() {
    private val LOGTAG = "gr.commonslab.sunnyclist"

    //Globals
    private lateinit var prefs: SharedPreferences

    // map embedded in the map fragment
    private lateinit var map: Map
    var s_ResultList: List<DiscoveryResult>? = null
    private lateinit var m_activity: AppCompatActivity
    private val m_placeDetailButton: Button? = null
    private val m_mapObjectList: List<MapObject> = ArrayList()

    // map fragment embedded in this activity
    private var mapFragment: AndroidXMapFragment = AndroidXMapFragment()
    private val REQUEST_CODE_ASK_PERMISSIONS = 1
    private val REQUIRED_SDK_PERMISSIONS = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE)

    private var paused: Boolean = false
    private var posManager: PositioningManager? = null
    private var m_mediumPowerMarker = false
    private var m_lowPowerMarker = false
    private var m_noPowerMarker = false

    // flag that indicates whether maps is being transformed
    private var mTransforming = false

    // callback that is called when transforming ends
    private var mPendingUpdate: Runnable? = null
    private var m_mapRoute: MapRoute? = null
    private var m_createRouteButton: Button? = null

    private lateinit var marker: MapMarker
    private var m_marker_image: Image? = null
    private val REQUEST_CODE_SEARCHRESULT = 333
    private val REQUEST_CODE_ROUTESELECTION = 334
    private lateinit var route: Route
    private lateinit var m_navigationManager: NavigationManager
    private lateinit var m_geoBoundingBox: GeoBoundingBox
    private var m_foregroundServiceStarted = false
    private var onForeground = true

    //Bluetooth
    lateinit var workerThread: Thread
    lateinit var readBuffer: ByteArray
    var readBufferPosition = 0
    var counter = 0
    var currentBTdata = emptyList<String>()
    var previousBTdata = emptyList<String>()

    var mCarDataAvailable = false
    var routeCalcNumberPassengers = 1
    var routeCalcStartTime = 0
    lateinit var configuration: JSONObject
    lateinit var carconf: JSONObject
    lateinit var solarconf: JSONObject
    lateinit var weatherconf: JSONObject
    lateinit var mapconf: JSONObject


    //Default configuration values
    //Car Defaults
    private var DEFAULT_CAR_WEIGHT = 300
    private var DEFAULT_PASSENGER_WEIGHT = 80
    private var DEFAULT_NUMBER_PASSENGERS = 1
    private var DEFAULT_MEAN_SPEED = 30
    private var DEFAULT_ROLLING_COEFFICIENT = 0.02
    private var DEFAULT_WEATHER_COEFFICIENT = 0.5

    private var DEFAULT_MAP_ZOOM = 15

    private var bmsSoc = 1.0

    fun initConfiguration() {
        try {
            val directory = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS) //Check if config.json exists
            if (!File("$directory/config.json").exists()) { //copy from assets
                // Opening data.json file
                val inputStream = assets.open("config.json")
                val size = inputStream.available()
                val buffer = ByteArray(size)
                var read: Int
                val out = FileOutputStream("$directory/config.json")
                while (inputStream.read(buffer).also { read = it } != -1) {
                    out.write(buffer, 0, read)
                }
                out.close()
                inputStream.close()
            } //Open file
            configuration = JSONObject(File("$directory/config.json").readText())
            Globals.debugMode = configuration.getJSONObject("configuration").getBoolean("debugMode")
            carconf = configuration.getJSONObject("configuration").getJSONObject("car")
            solarconf = configuration.getJSONObject("configuration").getJSONObject("solar")
            weatherconf = configuration.getJSONObject("configuration").getJSONObject("weather")
            mapconf = configuration.getJSONObject("configuration").getJSONObject("map")
            DEFAULT_NUMBER_PASSENGERS = carconf.getInt("NumberOfPassengers")
            DEFAULT_PASSENGER_WEIGHT = carconf.getInt("PassengersWeight")
            DEFAULT_CAR_WEIGHT = carconf.getInt("CarWeight")
            DEFAULT_MEAN_SPEED = carconf.getInt("MeanTravelSpeed")
            DEFAULT_ROLLING_COEFFICIENT = carconf.getDouble("RollingCoefficient")
            DEFAULT_MAP_ZOOM = configuration.getJSONObject("configuration").getJSONObject("map").getInt("InitialZoom")
            DEFAULT_WEATHER_COEFFICIENT = weatherconf.getDouble("WeatherCoefficient")
            with(prefs.edit()) {
                putFloat("InitialLat", mapconf.getDouble("InitialLat").toFloat())
                putFloat("InitialLon", mapconf.getDouble("InitialLon").toFloat())
                putFloat("InitialZoom", mapconf.getDouble("InitialZoom").toFloat())
                apply()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun bottomNavigationSetup() {
        val bottomNavigation: BottomNavigationView = findViewById<BottomNavigationView>(R.id.navigationView)
        val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                navigation_home       -> {
                    if (Globals.NavigationMode) {
                        m_navigationManager.stop();
                        map.zoomTo(m_geoBoundingBox, Map.Animation.NONE, 0f)
                        Globals.NavigationMode = false
                    }
                    centerMap()
                    return@OnNavigationItemSelectedListener true
                }
                navigation_search     -> {
                    val searchactivity = Intent(applicationContext, SearchActivity::class.java)
                    Globals.NavigationMode = false
                    posManager?.lastKnownPosition?.coordinate?.let { searchactivity.putExtra("mapcenter_long", it.longitude) }
                    posManager?.lastKnownPosition?.coordinate?.let { searchactivity.putExtra("mapcenter_lat", it.latitude) }
                    startActivityForResult(searchactivity, REQUEST_CODE_SEARCHRESULT)
                }
                navigation_navigation -> {
                    if (!Globals.NavigationMode) {
                        startNavigation(Globals.activeRoute)
                        bottomNavigation.selectedItemId = navigation_navigation
                    }
                    return@OnNavigationItemSelectedListener true
                }
                navigation_dashboard  -> {
                    Globals.NavigationMode = false
                    val dashboardactivity = Intent(applicationContext, DashboardActivity::class.java) //Pass map center to dashboard in order to be able to navigate to search from there too
                    dashboardactivity.putExtra("mapcenter_long", map.center.longitude)
                    dashboardactivity.putExtra("mapcenter_lat", map.center.latitude)
                    startActivity(dashboardactivity)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermissions()
        applicationContext.dataDir
        prefs = getSharedPreferences("gr.commonslab.sunnyclist", MODE_PRIVATE) //Load configuration

        initConfiguration()
        m_activity = this

        bottomNavigationSetup()

        //Bluetooth for getting data from the car
        Thread({
            if (findBT()) {
                Bluetooth.openBT()
            }
            Thread.sleep(10*60*1000)//10min
        }).start()
    }

    override fun onStart() {
        super.onStart()
        onForeground = true //Get car data from bluetooth every 5seconds
        Thread(Runnable { //TODO: handle no BT/data available case
            Log.i(LOGTAG, "MainActivity.BluetoothData")
            while (onForeground) {
                Thread.sleep(5000)
                previousBTdata = currentBTdata
                if (currentBTdata.size > 9) {
                    bmsSoc = currentBTdata[9].toDouble()
                }
                currentBTdata = readBTData()
            }
        }, "MainActivity.BluetoothData").start()
        initializeMap()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SEARCHRESULT) {
            if (resultCode == RESULT_OK) {
                map.removeAllMapObjects()
                Globals.availableRoutes.clear()
                routeConsumptions = arrayOf(0.0, 0.0, 0.0)
                routeDistances = arrayOf(0.0,0.0,0.0)
                if (data != null) { //get data
                    val fromlatitude = data.getDoubleExtra("from_latitude", 0.0)
                    val fromlongitude = data.getDoubleExtra("from_longitude", 0.0)
                    val fromtitle = data.getStringExtra("from_name")
                    dropMarker(GeoCoordinate(fromlatitude, fromlongitude, 0.0))
                    val destlatitude = data.getDoubleExtra("dest_latitude", 0.0)
                    val destlongitude = data.getDoubleExtra("dest_longitude", 0.0)
                    val desttitle = data.getStringExtra("dest_name")
                    dropMarker(GeoCoordinate(destlatitude, destlongitude, 0.0))
                    userDialogRouteCalculation(GeoCoordinate(fromlatitude, fromlongitude, 0.0), GeoCoordinate(destlatitude, destlongitude, 0.0)) //createRoutes(GeoCoordinate(fromlatitude, fromlongitude, 0.0), GeoCoordinate(destlatitude, destlongitude, 0.0))
                }
            }
        } else if (requestCode == REQUEST_CODE_ROUTESELECTION) {
            startNavigation(Globals.activeRoute)
        }
    }

    fun centerMap() {
        posManager?.lastKnownPosition?.let {
            map.setCenter(it.coordinate, Map.Animation.BOW)
        }
    }

    fun onPositionUpdated(locationMethod: LocationMethod?, geoPosition: GeoPosition, mapMatched: Boolean) {
        val coordinate = geoPosition.coordinate
        if (mTransforming) {
            mPendingUpdate = Runnable { onPositionUpdated(locationMethod, geoPosition, mapMatched) }
        } else {
            map.setCenter(coordinate, Map.Animation.BOW)
            if (locationMethod != null) {
                updateLocationInfo(locationMethod, geoPosition)
            }
        }
    }

    fun onPositionFixChanged(locationMethod: LocationMethod?, locationStatus: LocationStatus?) { // ignored
    }

    private fun handleOnMapLongPress() {

    }

    private fun initializeMap() { //setContentView(R.layout.activity_main)
        // Search for the map fragment to finish setup by calling init().
        mapFragment = (supportFragmentManager.findFragmentById(R.id.mapfragment) as AndroidXMapFragment?)!! // Set up disk map cache path for this application
        // Use path under your application folder for storing the disk cache
        setDiskCacheRootPath(applicationContext.getExternalFilesDir(null).toString() + File.separator.toString() + ".here-maps")
        mapFragment.init { error ->
            if (error == OnEngineInitListener.Error.NONE) { // retrieve a reference of the map from the map fragment
                map = mapFragment.map!!
                map.setCenter(GeoCoordinate(prefs.getFloat("InitialLat", 35.3387352F).toDouble(), prefs.getFloat("InitialLon", 25.1442126F).toDouble(), 0.0), Map.Animation.BOW)
                Globals.mainMap = mapFragment.map!!
                map.zoomLevel = prefs.getFloat("InitialZoom", 15.0F).toDouble()
                posManager = getInstance()
                centerMap()
                m_marker_image = Image()
                m_navigationManager = NavigationManager.getInstance()
                try {
                    m_marker_image!!.setImageResource(R.drawable.ic_map_marker);
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                mapFragment.mapGesture?.addOnGestureListener(object : MapGesture.OnGestureListener {
                    override fun onPanStart() { //showMsg("onPanStart")
                    }

                    override fun onPanEnd() {/* show toast message for onPanEnd gesture callback */ //showMsg("onPanEnd")
                    }

                    override fun onMultiFingerManipulationStart() {}
                    override fun onMultiFingerManipulationEnd() {}
                    override fun onMapObjectsSelected(list: List<ViewObject>): Boolean {
                        return false
                    }

                    override fun onTapEvent(p: PointF): Boolean {
                        val viewObjectList = map.getSelectedObjects(p) as ArrayList<ViewObject>
                        for (viewObject in viewObjectList) {
                            if (viewObject.baseType == ViewObject.Type.USER_OBJECT) {
                                val mapObject = viewObject as MapObject
                                if (mapObject.type == MapObject.Type.MARKER) {
                                    val selectedMarker = mapObject as MapMarker
                                    reverseGeocode(selectedMarker.coordinate)
                                }
                            }
                        }
                        return false
                    }

                    override fun onDoubleTapEvent(pointF: PointF): Boolean {
                        return false
                    }

                    override fun onPinchLocked() {}
                    override fun onPinchZoomEvent(v: Float, pointF: PointF): Boolean {
                        return false
                    }

                    override fun onRotateLocked() {}
                    override fun onRotateEvent(v: Float): Boolean {/* show toast message for onRotateEvent gesture callback */ //showMsg("onRotateEvent")
                        return false
                    }

                    override fun onTiltEvent(v: Float): Boolean {
                        return false
                    }

                    override fun onLongPressEvent(pointF: PointF): Boolean {
                        val position = map.pixelToGeo(pointF)
                        if (position != null) {
                            dropMarker(position)
                        }
                        paused = true
                        posManager?.lastKnownPosition?.let {
                            map.setCenter(it.coordinate, Map.Animation.BOW)
                        }
                        map.removeAllMapObjects()
                        Globals.availableRoutes.clear()
                        routeConsumptions = arrayOf(0.0, 0.0, 0.0)
                        routeDistances = arrayOf(0.0, 0.0, 0.0)
                        if (position != null) {
                            val socket = Bluetooth.mmSocket
                            if (socket == null) {
                                val alertDialog = AlertDialog.Builder(this@MainActivity).create()
                                alertDialog.setTitle("Unable to retrieve car status")
                                alertDialog.setMessage("No connection to car, assuming it is 100% charged.")
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK") {
                                    dialog, _ ->
                                    userDialogRouteCalculation(posManager!!.lastKnownPosition.coordinate, position)
                                    dialog.dismiss()
                                     }
                                alertDialog.show()
                            }
                        } else {
                            Toast.makeText(m_activity, "Missing destination, can not create route", Toast.LENGTH_LONG).show();
                        }
                        return false
                    }

                    override fun onLongPressRelease() {}
                    override fun onTwoFingerTapEvent(pointF: PointF): Boolean {
                        return false
                    }
                }, 0, false)
            } else {
                println("ERROR: Cannot initialize Map Fragment")
            }

            getInstance().addListener(WeakReference<OnPositionChangedListener>(positionListener)) // start position updates, accepting GPS, network or indoor positions
            if (posManager?.start(LocationMethod.GPS_NETWORK_INDOOR)!!) {
                map.positionIndicator.isVisible = true;
            } else {
                Toast.makeText(this, "PositioningManager.start: failed, exiting", Toast.LENGTH_LONG).show();
                finish();
            }
        } // display position indicator
        //mapFragment!!.positionIndicator?.setVisible(true);
    }

    private fun updateLocationInfo(locationMethod: LocationMethod, geoPosition: GeoPosition) {/*if (mLocationInfo == null) {
            return
        }*/
        val sb = StringBuffer()
        val coord = geoPosition.coordinate
        sb.append("Type: ").append(java.lang.String.format(Locale.US, "%s\n", locationMethod.name))
        sb.append("Coordinate:").append(java.lang.String.format(Locale.getDefault(), "%.6f, %.6f\n", coord.latitude, coord.longitude))
        if (coord.altitude != GeoCoordinate.UNKNOWN_ALTITUDE.toDouble()) {
            sb.append("Altitude:").append(java.lang.String.format(Locale.US, "%.2fm\n", coord.altitude))
        }
        if (geoPosition.heading != GeoPosition.UNKNOWN.toDouble()) {
            sb.append("Heading:").append(java.lang.String.format(Locale.US, "%.2f\n", geoPosition.heading))
        }
        if (geoPosition.speed != GeoPosition.UNKNOWN.toDouble()) {
            sb.append("Speed:").append(java.lang.String.format(Locale.US, "%.2fm/s\n", geoPosition.speed))
        }
        if (geoPosition.buildingName != null) {
            sb.append("Building: ").append(geoPosition.buildingName)
            if (geoPosition.buildingId != null) {
                sb.append(" (").append(geoPosition.buildingId).append(")\n")
            } else {
                sb.append("\n")
            }
        }
        if (geoPosition.floorId != null) {
            sb.append("Floor: ").append(geoPosition.floorId).append("\n")
        }
        sb.deleteCharAt(sb.length - 1) //mLocationInfo.setText(sb.toString())
    }


    private fun checkPermissions() {
        val missingPermissions: MutableList<String> = ArrayList() // check all required dynamic permissions
        for (permission in REQUIRED_SDK_PERMISSIONS) {
            val result = ContextCompat.checkSelfPermission(this, permission)
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission)
            }
        }
        if (missingPermissions.isNotEmpty()) { // request all missing permissions
            val permissions = missingPermissions.toTypedArray()
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS)
        } else {
            val grantResults = IntArray(REQUIRED_SDK_PERMISSIONS.size)
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED)
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults)
        }
    }

    fun dropMarker(position: GeoCoordinate) {
        if (::marker.isInitialized) {
            map.removeMapObject(marker)
        }
        marker = MapMarker()
        marker.coordinate = position
        map.addMapObject(marker)
    }

    fun reverseGeocode(position: GeoCoordinate) {
        val request = ReverseGeocodeRequest(position)
        request.execute { data, error ->
            if (error != ErrorCode.NONE) {
                Log.e("HERE", error.toString())
            } else {
                Toast.makeText(applicationContext, data?.address?.text, Toast.LENGTH_LONG).show()
            }
        }
    }

    // Resume positioning listener on wake up
    override fun onResume() {
        super.onResume()
        onForeground = true
        paused = false
        posManager?.start(LocationMethod.GPS_NETWORK)
        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView) //Set HOME or Navigation active in bottom menu
        if (Globals.NavigationMode) {
            bottomNavigation.selectedItemId = navigation_navigation
        } else {
            bottomNavigation.selectedItemId = navigation_home
        }
    }

    override fun onStop() {
        super.onStop()
        //closeBT()
        onForeground = false
    }

    // To pause positioning listener
    override fun onPause() {
        posManager?.stop()
        super.onPause()
        onForeground = false
        paused = true
    }

    // To remove the positioning listener
    override fun onDestroy() {
        posManager?.removeListener(positionListener)
        onForeground = false
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_ASK_PERMISSIONS -> {
                var index = permissions.size - 1
                while (index >= 0) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) { // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index] + "' not granted, exiting", Toast.LENGTH_LONG).show()
                        finish()
                        return
                    }
                    --index
                } // all permissions were granted
            }
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
    }


    private val positionListener: OnPositionChangedListener = object : OnPositionChangedListener {
        override fun onPositionUpdated(method: LocationMethod, position: GeoPosition?, isMapMatched: Boolean) { // set the center only when the app is in the foreground
            // to reduce CPU consumption
            /*if (!paused) {
                map!!.setCenter(
                    position!!.coordinate,
                    Map.Animation.NONE
                )
            }*/
        }

        override fun onPositionFixChanged(method: LocationMethod, status: LocationStatus) {
        }
    }

    private fun solarBeforeStart(lat: String, lon:String, duration: Int): Double {
        val ageCo = solarconf.getDouble("AgeCoefficient")
        val dustCo = solarconf.getDouble("DustCoefficient")
        val openRoof = solarconf.getDouble("ProductionOpenRoof")
        val closeRoof = solarconf.getDouble("ProductionCloseRoof")
        var roof = closeRoof //For current calculations assume roof is closed (Default)
        if (false) { //roof open
            roof = openRoof
        }
        val month = SimpleDateFormat("MM", Locale.US)
        val monthstr = month.format(Date(System.currentTimeMillis()))
        val hours = SimpleDateFormat("HH", Locale.US)
        val hour = hours.format(Date(System.currentTimeMillis())).toInt()
        var monthmulti = 1.0
        var hourmulti = 1.0
        val jsonMonths = solarconf.getJSONObject("months")
        when (monthstr) {
            "01" -> monthmulti = jsonMonths.getDouble("January")
            "02" -> monthmulti = jsonMonths.getDouble("February")
            "03" -> monthmulti = jsonMonths.getDouble("March")
            "04" -> monthmulti = jsonMonths.getDouble("April")
            "05" -> monthmulti = jsonMonths.getDouble("May")
            "06" -> monthmulti = jsonMonths.getDouble("June")
            "07" -> monthmulti = jsonMonths.getDouble("July")
            "08" -> monthmulti = jsonMonths.getDouble("August")
            "09" -> monthmulti = jsonMonths.getDouble("September")
            "10" -> monthmulti = jsonMonths.getDouble("October")
            "11" -> monthmulti = jsonMonths.getDouble("November")
            "12" -> monthmulti = jsonMonths.getDouble("December")
        }
        val jsonHours = solarconf.getJSONObject("hours")
        when {
            hour <= 7  -> hourmulti = jsonHours.getDouble("7")
            hour == 8  -> hourmulti = jsonHours.getDouble("8")
            hour == 9  -> hourmulti = jsonHours.getDouble("9")
            hour == 10 -> hourmulti = jsonHours.getDouble("10")
            hour == 11 -> hourmulti = jsonHours.getDouble("11")
            hour == 12 -> hourmulti = jsonHours.getDouble("12")
            hour == 13 -> hourmulti = jsonHours.getDouble("13")
            hour == 14 -> hourmulti = jsonHours.getDouble("14")
            hour == 15 -> hourmulti = jsonHours.getDouble("15")
            hour == 16 -> hourmulti = jsonHours.getDouble("16")
            hour == 17 -> hourmulti = jsonHours.getDouble("17")
            hour == 18 -> hourmulti = jsonHours.getDouble("18")
            hour >= 19 -> hourmulti = jsonHours.getDouble("19")
        }
        var timer = 0
        var weatherCo = 0.9
        while (WeatherAPI.waitingForResults) {
            WeatherAPI.getClouds(lat, lon)
            Thread.sleep(200)
            timer++
            if (timer > 5) //TODO: notify of no weather data
                break
            weatherCo = WeatherAPI.clouds[0]
        }
        return (ageCo * dustCo * weatherCo * roof * monthmulti * hourmulti * duration) / 1000 //kW/h
    }

    //Calculating Solar power production, distance in meters to calculate duration
    private fun calculateSolar(distance: Double, lat: String, lon: String, monthstr: String, hour: Int): Double {
        val meanSpeed = carconf.getDouble("MeanTravelSpeed") * 0.2777778 //m/s
        var duration = 80.0
        if (distance > 1000) {
            duration = distance / meanSpeed
        }
        val ageCo = solarconf.getDouble("AgeCoefficient")
        val dustCo = solarconf.getDouble("DustCoefficient")
        val openRoof = solarconf.getDouble("ProductionOpenRoof")
        val closeRoof = solarconf.getDouble("ProductionCloseRoof")
        var roof = closeRoof //For current calculations assume roof is closed (Default)
        if (false) { //roof open
            roof = openRoof
        }

        var monthmulti = 1.0
        var hourmulti = 1.0
        val jsonMonths = solarconf.getJSONObject("months")
        when (monthstr) {
            "01" -> monthmulti = jsonMonths.getDouble("January")
            "02" -> monthmulti = jsonMonths.getDouble("February")
            "03" -> monthmulti = jsonMonths.getDouble("March")
            "04" -> monthmulti = jsonMonths.getDouble("April")
            "05" -> monthmulti = jsonMonths.getDouble("May")
            "06" -> monthmulti = jsonMonths.getDouble("June")
            "07" -> monthmulti = jsonMonths.getDouble("July")
            "08" -> monthmulti = jsonMonths.getDouble("August")
            "09" -> monthmulti = jsonMonths.getDouble("September")
            "10" -> monthmulti = jsonMonths.getDouble("October")
            "11" -> monthmulti = jsonMonths.getDouble("November")
            "12" -> monthmulti = jsonMonths.getDouble("December")
        }
        val jsonHours = solarconf.getJSONObject("hours")
        when {
            hour <= 7  -> hourmulti = jsonHours.getDouble("7")
            hour == 8  -> hourmulti = jsonHours.getDouble("8")
            hour == 9  -> hourmulti = jsonHours.getDouble("9")
            hour == 10 -> hourmulti = jsonHours.getDouble("10")
            hour == 11 -> hourmulti = jsonHours.getDouble("11")
            hour == 12 -> hourmulti = jsonHours.getDouble("12")
            hour == 13 -> hourmulti = jsonHours.getDouble("13")
            hour == 14 -> hourmulti = jsonHours.getDouble("14")
            hour == 15 -> hourmulti = jsonHours.getDouble("15")
            hour == 16 -> hourmulti = jsonHours.getDouble("16")
            hour == 17 -> hourmulti = jsonHours.getDouble("17")
            hour == 18 -> hourmulti = jsonHours.getDouble("18")
            hour >= 19 -> hourmulti = jsonHours.getDouble("19")
        }
        var timer = 0
        var weatherCo = 0.9
        /*while (WeatherAPI.waitingForResults) {
            WeatherAPI.getClouds(lat, lon)
            Thread.sleep(200)
            timer++
            if (timer > 5) //TODO: notify of no weather data
                break
            weatherCo = WeatherAPI.clouds[0]
        }*/
        return (ageCo * dustCo * weatherCo * roof * monthmulti * hourmulti * (duration / 3600)) / 1000 //kWh
    }

    private fun calculateConsumption(distance: Double, angle: Double/*time that journey will start*/): Double { //Constants
        val rollingCoefficient = carconf.getDouble("RollingCoefficient")
        val G = 9.81 //m/s2
        val carWeight = carconf.getInt("CarWeight") //kg
        val passengerWeight = carconf.getInt("PassengersWeight") //kg
        val meanSpeed = carconf.getDouble("MeanTravelSpeed") * 0.2777778 //m/s //Configuration
        val mass = carWeight + (passengerWeight * routeCalcNumberPassengers) //kg
        val airDensity = carconf.getDouble("AirDensity") //kg/m3
        val dragCofficient = carconf.getDouble("DragCoefficient")
        val vehicleSectionArea = carconf.getDouble("VehicleSectionArea") //m2
        val aeroResistance = 0.5 * airDensity * dragCofficient * vehicleSectionArea * (meanSpeed * meanSpeed) //Newton
        val rollingResistance = rollingCoefficient * (mass * G * cos(angle)) //Newton
        val gradientResistance = mass * G * sin(angle) //Newton
        val force = aeroResistance + rollingResistance + gradientResistance
        return force * distance
    }

    private fun userDialogRouteCalculation(from: GeoCoordinate, dest: GeoCoordinate) {
        val builder: AlertDialog.Builder? = this.let { AlertDialog.Builder(it) }
        if (builder != null) {
            builder.setTitle("Route consumption")
            builder.setMessage("Type number of passengers and in how many hours are you planning to start your trip.")
        }
        val inputPassengers = EditText(this)
        inputPassengers.inputType = InputType.TYPE_CLASS_NUMBER
        inputPassengers.hint = "1 up to 3"
        val inputTime = EditText(this)
        inputTime.inputType = InputType.TYPE_CLASS_NUMBER
        inputTime.hint = "1 up to 48"
        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)

        inputPassengers.layoutParams = lp
        inputTime.layoutParams = lp
        val lila = LinearLayout(this)
        lila.orientation = LinearLayout.VERTICAL
        lila.addView(inputPassengers)
        lila.addView(inputTime)
        if (builder != null) {
            builder.setView(lila)
            builder.setPositiveButton("Submit", DialogInterface.OnClickListener { dialog, _ ->
                if (inputPassengers.text.toString().isEmpty()) {
                    routeCalcNumberPassengers = carconf.getInt("NumberOfPassengers")
                } else {
                    routeCalcNumberPassengers = inputPassengers.text.toString().toInt()
                }
                if (inputTime.text.toString().isEmpty()) {
                    routeCalcStartTime = 0
                } else {
                    routeCalcStartTime = inputTime.text.toString().toInt()
                }
                dialog.dismiss() //set default if either is empty
                createRoutes(from, dest)
            })

            builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, _ ->
                dialog.dismiss()
            })
            val alert: androidx.appcompat.app.AlertDialog = builder.create()
            alert.show()
            val button = alert.getButton(AlertDialog.BUTTON_POSITIVE)
            inputPassengers.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) { // my validation condition
                    if ((inputPassengers.text.length > 0) && (inputPassengers.text.toString().toInt() > 0) && (inputPassengers.text.toString().toInt() < 4)) {
                        button.isEnabled = true
                    } else {
                        button.isEnabled = false
                    }
                }

                override fun afterTextChanged(s: Editable) {}
            })
            inputTime.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) { // my validation condition
                    if ((inputTime.text.length > 0) && (inputTime.text.toString().toInt() > 0) && (inputTime.text.toString().toInt() < 49)) {
                        button.isEnabled = true
                    } else {
                        button.isEnabled = false
                    }
                }
                override fun afterTextChanged(s: Editable) {}
            })
        }
    }

    private fun createRoutes(from: GeoCoordinate, dest: GeoCoordinate) { //default num of pass 1, default time NOW
        val coreRouter = CoreRouter()
        val routePlan = RoutePlan()
        val routeOptions = RouteOptions() //routeOptions.setHighwaysAllowed(false)
        routeOptions.routeType = RouteOptions.Type.BALANCED
        routeOptions.routeCount = 3
        routeOptions.transportMode = RouteOptions.TransportMode.CAR
        routePlan.routeOptions = routeOptions/* Define waypoints for the route */
        val startPoint = RouteWaypoint(from)
        val destination = RouteWaypoint(dest)
        Globals.availableRoutes.clear()
        routeConsumptions = arrayOf(0.0, 0.0, 0.0)
        routeDistances = arrayOf(0.0,0.0,0.0)
        routePlan.removeAllWaypoints()
        routePlan.addWaypoint(startPoint)
        routePlan.addWaypoint(destination)
        val consumptionSpeed: MutableList<ConsumptionForSpeed> = ArrayList()
        consumptionSpeed.add(ConsumptionForSpeed(30, 5.0))
        consumptionSpeed.add(ConsumptionForSpeed(50, 10.0)) //consumptionSpeed.add(ConsumptionForSpeed(250, 27.41))
        val consumptionParams = ConsumptionParameters()
        consumptionParams.setSpeedParameters(consumptionSpeed)/* Trigger the route calculation,results will be called back via the listener */

        coreRouter.calculateRoute(routePlan, object : Router.Listener<List<RouteResult>, RoutingError> {
            override fun onProgress(i: Int) {/* The calculation progress can be retrieved in this callback. */
            }

            override fun onCalculateRouteFinished(p0: List<RouteResult>?, routingError: RoutingError) {
                if (p0 == null) {
                    return
                }
                var initialSolarProduction = 0.0
                var solarproduction = 0.0 //Calculate solar production at start and every 10km
                var routelog = ""/* Calculation is done. Let's handle the result */
                Globals.batteryCapacity = carconf.getDouble("BatteryCapacity") * bmsSoc

                if (routingError == RoutingError.NONE) {
                    if (routeCalcStartTime > 0) {
                        val location = p0[0].route.routeElements.elements[0].geometry[0]
                        initialSolarProduction = solarBeforeStart(location.latitude.toString(),location.longitude.toString(),routeCalcStartTime)
                        //reset starttime
                        routeCalcStartTime = 0
                    }
                    var totalConsumption = 0.0
                    totalConsumption -= initialSolarProduction
                    for (i in p0.indices) {/* Create a MapRoute so that it can be placed on the map */
                        MapRoute.RenderType.USER_DEFINED
                        m_mapRoute = MapRoute(p0[i].route)
                        route = p0[i].route
                        Globals.availableRoutes.add(route)
                        val elevationData = route.routeGeometryWithElevationData
                        distancefromElevationdata(elevationData)
                        var segment = 1
                        var totalDistance = 0.0
                        var totalDistanceOutOfRange = 0.0 //Use route consumption for each route element
                        val routeElements = route.routeElements.elements
                        var solarDistance = 0.0
                        for (x in routeElements.indices) {
                            val element = routeElements[x] //val geometry = element.geometry
                            for (g in element.geometry.indices) {
                                if (g == 0) continue
                                val elevationDiff = element.geometry[g].altitude - element.geometry[g - 1].altitude
                                val distance = distance(element.geometry[g - 1], element.geometry[g])
                                totalDistance += distance
                                val angle = atan(elevationDiff / distance) //radians
                                val consumption = calculateConsumption(distance, angle) //Joules
                                if ((solarDistance == 0.0) || (solarDistance > 10000)) {
                                    routelog += String.format("Calculate solar production for distance at total distance:%.2f, %.2f\n", solarDistance, totalDistance)
                                    val month = SimpleDateFormat("MM", Locale.US)
                                    val monthstr = month.format(Date(System.currentTimeMillis()))
                                    val hours = SimpleDateFormat("HH", Locale.US)
                                    var hourOfDay = hours.format(Date(System.currentTimeMillis())).toInt() + routeCalcStartTime
                                    while (hourOfDay > 24) {
                                        hourOfDay -= 24
                                    }
                                    routelog += String.format("Solar production at month,time:%s,%d\n", monthstr, hourOfDay)
                                    solarproduction = calculateSolar(solarDistance, element.geometry[g].latitude.toString(), element.geometry[g].longitude.toString(), monthstr, hourOfDay)
                                    routelog += String.format("Solar production:%.2f\n", solarproduction)
                                    solarDistance = 0.0
                                    totalConsumption -= solarproduction
                                }
                                solarDistance += distance
                                totalConsumption += consumption / 3600000 // kW/h
                                if (totalConsumption > Globals.batteryCapacity * 0.95) {
                                    totalDistanceOutOfRange += distance
                                }
                                routelog += String.format("%d: Distance:%.2f Angle:%f Consumption:%.2f Total Distance:%.2f, Total consumption:%.2f\n", segment, distance, angle, consumption, totalDistance, totalConsumption)
                                segment++
                            }

                            //Add markers only on last (preferred and blue drawn) route
                            if (i + 1 == p0.size) {
                                if (!m_mediumPowerMarker && (totalConsumption > Globals.batteryCapacity * 0.5)) {
                                    val location = GeoCoordinate(element.geometry[0].latitude, element.geometry[0].longitude)
                                    addConsumptionMarker(50, location)
                                } else if (!m_lowPowerMarker && (totalConsumption > Globals.batteryCapacity * 0.8)) {
                                    val location = GeoCoordinate(element.geometry[0].latitude, element.geometry[0].longitude)
                                    addConsumptionMarker(20, location)
                                } else if (!m_noPowerMarker && (totalConsumption > Globals.batteryCapacity * 0.95)) { //5%
                                    val location = GeoCoordinate(element.geometry[0].latitude, element.geometry[0].longitude)
                                    addConsumptionMarker(0, location)
                                    val outOfRangeMsg = String.format("Car battery may run out with %.0f km left for your trip.", totalDistanceOutOfRange) //Notify user that route, car battery will be out of range
                                    val alertDialog = AlertDialog.Builder(this@MainActivity).create()
                                    alertDialog.setTitle("Out of range")
                                    alertDialog.setMessage(outOfRangeMsg)
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK") { dialog, _ -> dialog.dismiss() }
                                    alertDialog.show()
                                } /*else { //Notify user of remaining km in range
                                    val remainingBattery = (batteryCapacity * 0.95) - totalConsumption
                                    val meanConsumption = totalConsumption / totalDistance
                                    val range = remainingBattery / meanConsumption
                                    val remainingRange = String.format("Car will have approximately %.0f km more range at end of trip.", range)
                                    val alertDialog = AlertDialog.Builder(this@MainActivity).create()
                                    alertDialog.setTitle("Remaining range")
                                    alertDialog.setMessage(remainingRange)
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK") { dialog, _ -> dialog.dismiss() }
                                    alertDialog.show()
                                }*/
                            }
                        }
                        routeConsumptions[i] = totalConsumption
                        routeDistances[i] = totalDistance
                        storeRoute(applicationContext, routelog)/* Show the maneuver number on top of the route */
                        m_mapRoute!!.isManeuverNumberVisible = true

                        /* Add the MapRoute to the map */
                        when (i) {
                            0 -> { m_mapRoute!!.color = Color.BLUE }
                            1 -> { m_mapRoute!!.color = Color.GRAY }
                            2 -> { m_mapRoute!!.color = Color.GREEN }
                        }
                        map.addMapObject(m_mapRoute!!)
                    } //for routes loop
                    m_geoBoundingBox = route.boundingBox!!
                    map.zoomTo(m_geoBoundingBox, Map.Animation.LINEAR, Map.MOVE_PRESERVE_ORIENTATION) //Show route selection dialog
                    if (p0.size > 1) {
                        val routeSelectionFragment = RouteSelectionFragment()
                        routeSelectionFragment.show(supportFragmentManager, "") //fragment_layout.alpha = 1.0f
                    } else {
                        remainingDistance()
                    }
                } else {
                    Toast.makeText(applicationContext, "Error:route calculation returned error code: $routingError", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    fun remainingDistance() {
        val remainingBattery = (Globals.batteryCapacity * 0.95) - Globals.routeConsumptions[Globals.selectedRoute]
        val meanConsumption = Globals.routeDistances[Globals.selectedRoute]/Globals.routeConsumptions[Globals.selectedRoute]
        val range = remainingBattery / meanConsumption
        val pedalProduction = carconf.getInt("PedalProduction")
        val meanSpeed = carconf.getInt("MeanTravelSpeed")
        val duration = Globals.routeDistances[Globals.selectedRoute] / meanSpeed
        val pedalTotalProduction = duration * pedalProduction
        val pedalRange = pedalTotalProduction/meanConsumption
        val remainingRange = String.format("Car will have approximately %.0f km more range at end of trip. You will gain extra %.1f km for using each pedal during your trip.", range, pedalRange)
        val alertDialog = AlertDialog.Builder(this@MainActivity).create()
        alertDialog.setTitle("Remaining range")
        alertDialog.setMessage(remainingRange)
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK") { dialog, _ -> dialog.dismiss() }
        alertDialog.show()
    }

    fun storeRoute(ctx: Context, route: String) {
        val fname = "routelog.txt"
        //Do not append
        File(ctx.filesDir, fname).writeText(route)
        //TODO:Debug File(ctx.filesDir, fname).appendText(route)
    }

    val discoveryResultPageListener: ResultListener<DiscoveryResultPage> = object : ResultListener<DiscoveryResultPage> {
        override fun onCompleted(discoveryResultPage: DiscoveryResultPage?, errorCode: ErrorCode?) {
            if (errorCode == ErrorCode.NONE) {/* No error returned,let's handle the results */
                m_placeDetailButton?.visibility = View.VISIBLE

                /*
         * The result is a DiscoveryResultPage object which represents a paginated
         * collection of items.The items can be either a PlaceLink or DiscoveryLink.The
         * PlaceLink can be used to retrieve place details by firing another
         * PlaceRequest,while the DiscoveryLink is designed to be used to fire another
         * DiscoveryRequest to obtain more refined results.
         */if (discoveryResultPage != null) {
                    s_ResultList = discoveryResultPage.items
                }
                for (item in s_ResultList!!) {/*
                 * Add a marker for each result of PlaceLink type.For best usability, map can be
                 * also adjusted to display all markers.This can be done by merging the bounding
                 * box of each result and then zoom the map to the merged one.
                 */
                    if (item.resultType == DiscoveryResult.ResultType.PLACE) {
                        val placeLink = item as PlaceLink
                        addMarkerAtPlace(placeLink)
                    }
                }
            } else {
                Toast.makeText(applicationContext, "ERROR:Discovery search request returned return error code+ $errorCode", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun addConsumptionMarker(percent: Int, coords: GeoCoordinate) {
        var customMarker: MapMarker? = null
        val image = Image()
        if (percent > 50) {
            return
        } else if (percent > 20) { //50%
            image.setImageResource(R.drawable.map_marker_50)
            customMarker = MapMarker(coords, image)
            m_mediumPowerMarker = true
        } else if (percent > 0) { //< 20%
            image.setImageResource(R.drawable.map_marker_20)
            customMarker = MapMarker(coords, image)
            m_lowPowerMarker = true
        } else { //0%
            image.setImageResource(R.drawable.map_marker_0)
            customMarker = MapMarker(coords, image)
            m_noPowerMarker = true
        }

        map.addMapObject(customMarker)
        m_mapObjectList.plus(customMarker)
    }

    private fun addMarkerAtPlace(placeLink: PlaceLink) {
        val img = Image()
        try {
            img.setImageResource(R.drawable.ic_location_on)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        val mapMarker = MapMarker()
        mapMarker.icon = img
        mapMarker.coordinate = GeoCoordinate(placeLink.position!!)
        map.addMapObject(mapMarker)
        m_mapObjectList.plus(mapMarker)
    }

    open fun startForegroundService() {
        if (!m_foregroundServiceStarted) {
            m_foregroundServiceStarted = true
            val startIntent = Intent(m_activity, NavigationForegroundService::class.java)
            startIntent.action = NavigationForegroundService.START_ACTION
            m_activity.applicationContext.startService(startIntent)
        }
    }

    open fun stopForegroundService() {
        if (m_foregroundServiceStarted) {
            m_foregroundServiceStarted = false
            val stopIntent = Intent(m_activity, NavigationForegroundService::class.java)
            stopIntent.action = NavigationForegroundService.STOP_ACTION
            m_activity.applicationContext.startService(stopIntent)
        }
    }

    fun startNavigation(route: Route) {
        Globals.NavigationMode = true //m_naviControlButton.setText(R.string.stop_navi)
        /* Configure Navigation manager to launch navigation on current map */
        m_navigationManager.setMap(map) // show position indicator
        // note, it is also possible to change icon for the position indicator
        mapFragment.positionIndicator?.isVisible = true
        m_navigationManager.startNavigation(route)
        map.tilt = 60F
        startForegroundService()
        val bottomNavigation = findViewById<BottomNavigationView>(R.id.navigationView)
        bottomNavigation.selectedItemId = navigation_navigation
        m_navigationManager.mapUpdateMode = MapUpdateMode.ROADVIEW
        addNavigationListeners()
    }

    open fun addNavigationListeners() {

        /*
         * Register a NavigationManagerEventListener to monitor the status change on
         * NavigationManager
         */
        m_navigationManager.addNavigationManagerEventListener(WeakReference(m_navigationManagerEventListener))

        /* Register a PositionListener to monitor the position updates */
        m_navigationManager.addPositionListener(WeakReference(m_positionListener))
    }

    private val m_positionListener: NavigationManager.PositionListener = object : NavigationManager.PositionListener() {
        override fun onPositionUpdated(geoPosition: GeoPosition) {/* Current position information can be retrieved in this callback */
        }
    }

    private val m_navigationManagerEventListener: NavigationManagerEventListener = object : NavigationManagerEventListener() {
        override fun onRunningStateChanged() {
            if (Globals.debugMode) {
                Toast.makeText(m_activity, "Running state changed", Toast.LENGTH_SHORT).show()
            }
        }

        override fun onNavigationModeChanged() {
            if (Globals.debugMode) {
                Toast.makeText(m_activity, "Navigation mode changed", Toast.LENGTH_SHORT).show()
            }
        }

        override fun onEnded(navigationMode: NavigationManager.NavigationMode) {
            if (Globals.debugMode) {
                Toast.makeText(m_activity, "$navigationMode was ended", Toast.LENGTH_SHORT).show()
            }
            stopForegroundService()
            Globals.NavigationMode = false
        }

        override fun onMapUpdateModeChanged(mapUpdateMode: MapUpdateMode) {
            Toast.makeText(m_activity, "Map update mode is changed to $mapUpdateMode", Toast.LENGTH_SHORT).show()
        }

        override fun onCountryInfo(s: String, s1: String) {
            Toast.makeText(m_activity, "Country info updated from $s to $s1", Toast.LENGTH_SHORT).show()
        }
    }

    fun distancefromElevationdata(elevationdata: MutableList<GeoCoordinate>): Double {
        var totaldistance = 0.0
        for (i in 0 until elevationdata.count() - 1) {
            totaldistance += distance(elevationdata[i], elevationdata[i + 1])
        }
        return totaldistance
    }

    fun distance(from: GeoCoordinate, to: GeoCoordinate): Double {
        val r: Double = 6375000.0
        val latitude: Double = (to.latitude - from.latitude) * (PI / 180)
        val longtitude: Double = (to.longitude - from.longitude) * (PI / 180)
        val a = sin(latitude / 2).pow(2) + cos(from.latitude) * cos(to.latitude) * sin(longtitude / 2).pow(2)
        return r * (2 * asin(sqrt(a)))
    }

    override fun onBackPressed() { //See if it has return from route selection dialog, then start navigation
        if (Globals.routeSelectionResult) {
            Globals.routeSelectionResult = false
            startNavigation(Globals.activeRoute)
            return
        } else {
            super.onBackPressed()
        }
    }

    open fun readBTData(): List<String> {
        val delimiter: Byte = 10 //This is the ASCII code for a newline character
        readBufferPosition = 0
        readBuffer = ByteArray(1024)
        var speed = 32
        var totaldistance = 89
        var pwr_slrCurrentPower = 22
        var pwr_slrTotalPower = 23
        var pwr_pdl1CurrentPower = 1
        var pwr_pdl1TotalPower = 12
        var pwr_pdl2CurrentPower = 0
        var pwr_pdl2TotalPower = 123
        var bms_currentAvgPower = 6
        var bms_soc = 43
        var bms_totalConsumed = 12
        var bms_currentPower = 12
        var parkingStatus = 1
        var warningStatus = 0
        var errormsg = "" //DEBUG
        //DEBUG
        //var dummydata = "$speed:$totaldistance:$pwr_slrCurrentPower:$pwr_slrTotalPower:$pwr_pdl1CurrentPower:$pwr_pdl1TotalPower:$pwr_pdl2CurrentPower:$pwr_pdl2TotalPower:$bms_currentAvgPower:$bms_soc:$bms_totalConsumed:$bms_currentPower:$parkingStatus:$warningStatus:$errormsg".split(":")
        //return dummydata
        try {
            if (!Bluetooth.initialized) {
                return emptyList()
            }
            val bytesAvailable = Bluetooth.mmInputStream.available()
            if (bytesAvailable > 0) {
                val packetBytes = ByteArray(bytesAvailable)
                Bluetooth.mmInputStream.read(packetBytes)
                for (i in 0 until bytesAvailable) {
                    val b = packetBytes[i]
                    if (b == delimiter) {
                        val encodedBytes = ByteArray(readBufferPosition)
                        System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.size)
                        val strdata = String(encodedBytes, Charset.defaultCharset())
                        //speed,totaldistance,pwr_slrCurrentPower,pwr_slrTotalPower,pwr_pdl1CurrentPower,pwr_pdl1TotalPower,pwr_pdl2CurrentPower,pwr_pdl2TotalPower,bms_currentAvgPower,bms_soc,bms_totalConsumed
                        return strdata.split(":")
                    } else {
                        readBuffer[readBufferPosition++] = b
                    }
                }
            }
        } catch (ex: IOException) {
            return emptyList<String>()
        }
        return emptyList<String>()
    }

    open fun findBT(): Boolean {
        Bluetooth.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (!Bluetooth.mBluetoothAdapter.isEnabled) {
            val enableBluetooth = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBluetooth, 0)
            return false
        }
        val pairedDevices = Bluetooth.mBluetoothAdapter.bondedDevices
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                if (device.name == "Sunnyclist") {
                    Bluetooth.mmDevice = device
                    return true
                }
            }
        }
        return false
    }

    /*@Throws(IOException::class)
    open fun openBT() {
        val uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        Bluetooth.mmSocket = Bluetooth.mmDevice.createRfcommSocketToServiceRecord(uuid)
        try {
            val socket = Bluetooth.mmSocket
            if (socket == null) {
                return
            }
            socket.connect()
            Bluetooth.mmOutputStream = socket.outputStream
            Bluetooth.mmInputStream = socket.inputStream //beginListenForData()
            mCarDataAvailable = true
        } catch (e: IOException) {
            mCarDataAvailable = false
            Toast.makeText(this, "Connection to Sunnyclist is not currently available.", Toast.LENGTH_LONG).show();
        }
    }

    @Throws(IOException::class)
    open fun closeBT() {
        Bluetooth.mmOutputStream.close()
        Bluetooth.mmInputStream.close()
        Bluetooth.mmSocket!!.close()
    }*/

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.settings, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_settings) {
            val settingIntent = Intent(applicationContext, SettingsActivity::class.java)
            startActivity(settingIntent)
            return true
        } else if (item.itemId == R.id.action_info) {
            val InfoIntent = Intent(applicationContext, InfoActivity::class.java)
            startActivity(InfoIntent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
