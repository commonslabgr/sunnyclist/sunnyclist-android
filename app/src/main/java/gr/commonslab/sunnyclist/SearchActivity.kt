package gr.commonslab.sunnyclist

import SearchListAdapter
import android.app.ListActivity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.here.android.mpa.common.GeoCoordinate
import com.here.android.mpa.search.*
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.content_search_list.*


class SearchActivity : ListActivity() {
    var dataList = ArrayList<HashMap<String, String>>()
    var adapter: SearchListAdapter? = null
    private var m_placeDetailLayout: LinearLayout? = null
    private var m_placeName: TextView? = null
    private var m_placeLocation: TextView? = null
    private var onSearchView = ""
    private var fromSet = false
    private var destSet = false
    private val returnIntent: Intent = Intent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val radiousSearch = 350000
        val param = intent.extras
        var longitude: Double = 0.0
        longitude = param?.getDouble("mapcenter_long")!!
        var latitude: Double = 0.0
        latitude =  param?.getDouble("mapcenter_lat")
        val center: GeoCoordinate = GeoCoordinate(latitude, longitude)

        //Initialize from point
        fromSet = true
        returnIntent.putExtra("from_latitude", center.latitude)
        returnIntent.putExtra("from_longitude", center.longitude)

        val fromsearchView = findViewById<SearchView>(R.id.fromsearchText)
        fromsearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                dataList.clear()
                if (newText.length < 3) return false
                val searchRequest = SearchRequest(newText)
                if (searchRequest != null) {
                    searchRequest.setSearchCenter(center)
                    searchRequest.setSearchArea(center,radiousSearch)
                    searchRequest.execute(searchResultPageListener)
                }
                onSearchView = "from"
                return false
            }
        })

        val destsearchView = findViewById<SearchView>(R.id.destinationsearchText)
        destsearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                dataList.clear()
                if (newText.length < 3) return false
                val searchRequest = SearchRequest(newText)
                if (searchRequest != null) {
                    searchRequest.setSearchCenter(center)
                    searchRequest.setSearchArea(center,radiousSearch)//radious of 100km
                    searchRequest.execute(searchResultPageListener)
                }
                onSearchView = "destination"
                return false
            }
        })

        val bottomNavigation: BottomNavigationView = findViewById<BottomNavigationView>(R.id.navigationView)
        bottomNavigation.selectedItemId = R.id.navigation_search
        val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    finish()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_search -> {
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_navigation -> {
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_dashboard -> {
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    val searchResultPageListener: ResultListener<DiscoveryResultPage> =
        object : ResultListener<DiscoveryResultPage> {
            override fun onCompleted(
                discoveryResultPage: DiscoveryResultPage?,
                errorCode: ErrorCode?
            ) {
                if (errorCode == ErrorCode.NONE) {
                    if (discoveryResultPage != null) {

                        for (item in discoveryResultPage.items) {
                            if (item.getResultType() == DiscoveryResult.ResultType.PLACE) {
                                var placeLink: PlaceLink = item as PlaceLink
                                val results = HashMap<String, String>()
                                results["title"] = item.title
                                results["id"] = item.id
                                results["vicinity"] = item.vicinity
                                results["latitude"] = item.position?.latitude.toString()
                                results["longitude"] = item.position?.longitude.toString()
                                dataList.add(results)
                            } else if (item.getResultType() == DiscoveryResult.ResultType.DISCOVERY) {
                                var discoveryLink: DiscoveryLink = item as DiscoveryLink
                                val results = HashMap<String, String>()
                                results["title"] = item.title
                                results["id"] = item.id
                                results["vicinity"] = item.vicinity
                                results["latitude"] = (item as PlaceLink).position?.latitude.toString()
                                results["longitude"] = (item as PlaceLink).position?.longitude.toString()
                                dataList.add(results)
                            }

                        }
                        adapter = SearchListAdapter(this@SearchActivity, dataList)
                        findViewById<ListView>(android.R.id.list).adapter = adapter
                    }
                } else {
                    Toast.makeText(
                        applicationContext,
                        "ERROR:Discovery search request returned return error code+ $errorCode",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

    /* Retrieve details of the place selected */
    override fun onListItemClick(l: ListView?, v: View?, position: Int, id: Long) {
        val result = dataList.get(position)
        if (onSearchView == "from") {
            fromSet = true
            returnIntent.putExtra("from_latitude", result["latitude"]?.toDouble())
            returnIntent.putExtra("from_longitude", result["longitude"]?.toDouble())
            returnIntent.putExtra("from_name", result["title"])
            fromsearchText.setQuery(result["title"], false)
            dataList.clear()
        } else {
            destSet = true
            destinationsearchText.setQuery(result["title"], false)
            returnIntent.putExtra("dest_latitude", result["latitude"]?.toDouble())
            returnIntent.putExtra("dest_longitude", result["longitude"]?.toDouble())
            returnIntent.putExtra("dest_name", result["title"])
            dataList.clear()
        }

        if ((fromSet) && (destSet)) {
            setResult(RESULT_OK, returnIntent)
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private val m_placeResultListener: Any? =
    ResultListener<Any?> { place, errorCode ->
        fun onCompleted(place: Place, errorCode: ErrorCode) {
            if (errorCode === ErrorCode.NONE) {
                /*
                     * No error returned,let's show the name and location of the place that just being
                     * selected.Additional place details info can be retrieved at this moment as well,
                     * please refer to the HERE Android SDK API doc for details.
                     */
                m_placeDetailLayout!!.visibility = View.VISIBLE
                m_placeName!!.text = place.name
                val geoCoordinate: GeoCoordinate? = place.location!!.coordinate
                m_placeLocation?.text = geoCoordinate.toString()
            } else {
                Toast.makeText(
                    applicationContext,
                    "ERROR:Place request returns error: $errorCode", Toast.LENGTH_SHORT
                )
                    .show()
            }
        }
    }
}