package gr.commonslab.sunnyclist

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Intent
import android.widget.Toast
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.*

object Bluetooth {
    lateinit var mBluetoothAdapter: BluetoothAdapter
    lateinit var mmSocket: BluetoothSocket
    lateinit var mmDevice: BluetoothDevice
    lateinit var mmOutputStream: OutputStream
    lateinit var mmInputStream: InputStream
    var initialized = false


    fun findBT(): Boolean {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (!mBluetoothAdapter.isEnabled) {
            //TODO: notify user to change settings
            return false
        }
        val pairedDevices = Bluetooth.mBluetoothAdapter.bondedDevices
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                if (device.name == "Sunnyclist") {
                    mmDevice = device
                    return true
                }
            }
        }
        return false
    }

    @Throws(IOException::class)
    fun openBT() {
        val uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        if (!::mmSocket.isInitialized) {
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid)
        }
        if (!mmSocket.isConnected) {
            try {
                mmSocket.connect()
                mmOutputStream = mmSocket.outputStream
                mmInputStream = mmSocket.inputStream
                initialized = true
            } catch (e: IOException) {
                initialized = false
                //Toast.makeText(this, "Connection to Sunnyclist is not currently available.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Throws(IOException::class)
    fun closeBT() {
        mmOutputStream.close()
        mmInputStream.close()
        mmSocket.close()
    }
}