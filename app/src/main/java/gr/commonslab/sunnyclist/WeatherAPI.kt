package gr.commonslab.sunnyclist

import android.os.AsyncTask
import com.here.android.mpa.common.GeoCoordinate
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.TimeUnit

object WeatherAPI {
    var clouds = arrayOf(0.9,0.9)// set how many hours to get
    var waitingForResults = false

    class getForecast(): AsyncTask<String, Void, String>() {
        private val FORECAST_URL = "https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&exclude=alerts&appid="
        private val APIKEY = "1229e000c3fc0f6f15ec3a92f1fa8843"

        fun callWeatherAPI(latitude:String,longitude:String) :String {
            var buffer = StringBuffer()
            waitingForResults = true
            var result = ""
            var url = FORECAST_URL+APIKEY
            url = url.replace("{lat}",latitude)
            url = url.replace("{lon}",longitude)
            var con:HttpURLConnection = URL(url).openConnection() as HttpURLConnection
            try {
                con.requestMethod = "GET"
                con.connect()
                if(con.responseCode == 200) {
                    result = con.inputStream.bufferedReader().readText()
                }
                con.disconnect()
            }catch (e: Exception) {
                result = e.printStackTrace().toString()
            }
            return result
        }

        override fun doInBackground(vararg params: String): String? {
            val resp = callWeatherAPI(params[0], params[1])
            return resp
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            waitingForResults = false
            if (!result.isNullOrBlank()) {
                val jsonObj = JSONObject(result)
                if (jsonObj.has("hourly")) {
                    val hourly = jsonObj.getJSONArray("hourly")
                    for (i in clouds.indices) {
                        clouds[i] = ((100-hourly.getJSONObject(i).getInt("clouds"))/100).toDouble()
                    }
                }
            }
        }
    }

    fun getClouds(lat:String, lon: String) {
        getForecast().execute(lat,lon)
    }
}