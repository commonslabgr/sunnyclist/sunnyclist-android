package gr.commonslab.sunnyclist

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.here.android.mpa.mapping.MapRoute
import org.json.JSONObject
import java.io.File

class RouteSelectionFragment : BottomSheetDialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.route_selection_layout, container, false)
        val directory = context?.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
        //Open file
        val configuration = JSONObject(File("$directory/config.json").readText())
        //Update default values
        val carconf = configuration.getJSONObject("configuration").getJSONObject("car")
        val navView1 = v.findViewById<Button>(R.id.routeselection1_navview)
        navView1.setOnClickListener {
            val mapRoute = MapRoute(Globals.availableRoutes[0])
            Globals.mainMap.removeAllMapObjects()
            Globals.mainMap.addMapObject(mapRoute)
        }
        val navStart1 = v.findViewById<Button>(R.id.routeselection1_navstart)
        navStart1.setOnClickListener {
            Globals.activeRoute = Globals.availableRoutes[0]
            Globals.routeSelectionResult = true
            remainingDistance()
            activity?.onBackPressed()
            dismiss()
        }
        val r1distance = v.findViewById<TextView>(R.id.routeselection1_distance)
        val r1time = v.findViewById<TextView>(R.id.routeselection1_time)
        val r1consumption = v.findViewById<TextView>(R.id.routeselection1_consumption)
        var distance = Globals.availableRoutes[0].length.toFloat()
        var consumption = Globals.routeConsumptions[0]
        if (distance > 1000) {
            distance /= 1000
            r1distance.text = "$distance km"
        } else {
            r1distance.text = "$distance m"
        }
        var time = distance / carconf.getInt("MeanTravelSpeed")
        r1time.text = "%.2f min".format(time)
        r1consumption.text = "%.2f kW".format(consumption)
        val navView2 = v.findViewById<Button>(R.id.routeselection2_navview)
        navView2.setOnClickListener {
            val mapRoute = MapRoute(Globals.availableRoutes[1])
            Globals.mainMap.removeAllMapObjects()
            Globals.mainMap.addMapObject(mapRoute)
        }
        val navStart2 = v.findViewById<Button>(R.id.routeselection2_navstart)
        navStart2.setOnClickListener {
            Globals.activeRoute = Globals.availableRoutes[1]
            Globals.routeSelectionResult = true
            Globals.selectedRoute = 1
            activity?.onBackPressed()
            remainingDistance()
            dismiss()
        }
        val r2distance = v.findViewById<TextView>(R.id.routeselection2_distance)
        val r2time = v.findViewById<TextView>(R.id.routeselection2_time)
        val r2consumption = v.findViewById<TextView>(R.id.routeselection2_consumption)
        distance = Globals.availableRoutes[1].length.toFloat()
        consumption = Globals.routeConsumptions[1]
        if (distance > 1000) {
            distance /= 1000
            r2distance.text = "$distance km"
        } else {
            r2distance.text = "$distance m"
        }
        time = distance / carconf.getInt("MeanTravelSpeed")
        r2time.text = "%.2f min".format(time)
        r2consumption.text = "%.2f kW".format(consumption)
        if (Globals.availableRoutes.size == 2) {
            val route3 = v.findViewById<LinearLayout>(R.id.routeselection_3)
            route3.visibility = View.INVISIBLE
        } else {//3 routes
            val navView3 = v.findViewById<Button>(R.id.routeselection3_navview)
            navView3.setOnClickListener {
                val mapRoute = MapRoute(Globals.availableRoutes[2])
                Globals.mainMap.removeAllMapObjects()
                Globals.mainMap.addMapObject(mapRoute)
            }
            val navStart3 = v.findViewById<Button>(R.id.routeselection3_navstart)
            navStart3.setOnClickListener {
                Globals.activeRoute = Globals.availableRoutes[2]
                Globals.routeSelectionResult = true
                Globals.selectedRoute = 2
                activity?.onBackPressed()
                remainingDistance()
                dismiss()
            }
            val r3distance = v.findViewById<TextView>(R.id.routeselection3_distance)
            val r3time = v.findViewById<TextView>(R.id.routeselection3_time)
            val r3consumption = v.findViewById<TextView>(R.id.routeselection3_consumption)
            distance = Globals.availableRoutes[2].length.toFloat()
            consumption = Globals.routeConsumptions[2]
            if (distance > 1000) {
                distance /= 1000
                r3distance.text = "$distance km"
            } else {
                r3distance.text = "$distance m"
            }
            time = distance / carconf.getInt("MeanTravelSpeed")
            r3time.text = "%.2f min".format(time)
            r3consumption.text = "%.2f kW".format(consumption)
        }
        return v
    }

    private fun remainingDistance() {
        val remainingBattery = (Globals.batteryCapacity * 0.95) - Globals.routeConsumptions[Globals.selectedRoute]
        val meanConsumption = Globals.routeDistances[Globals.selectedRoute]/Globals.routeConsumptions[Globals.selectedRoute]
        val range = remainingBattery / meanConsumption
        val directory = requireActivity().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
        val configuration = JSONObject(File("$directory/config.json").readText())
        val pedalProduction = configuration.getJSONObject("configuration").getJSONObject("car").getInt("PedalProduction")
        val meanSpeed = configuration.getJSONObject("configuration").getJSONObject("car").getInt("MeanTravelSpeed")
        val duration = Globals.routeDistances[Globals.selectedRoute] / meanSpeed
        val pedalTotalProduction = duration * pedalProduction
        val pedalRange = pedalTotalProduction/meanConsumption
        val remainingRange = String.format("Car will have approximately %.0f km more range at end of trip. You will gain extra %.1f km for using each pedal during your trip.", range, pedalRange)
        val alertDialog = AlertDialog.Builder(this.requireActivity()).create()
        alertDialog.setTitle("Remaining range")
        alertDialog.setMessage(remainingRange)
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK") { dialog, _ -> dialog.dismiss() }
        alertDialog.show()
    }
}