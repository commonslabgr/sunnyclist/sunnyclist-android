package gr.commonslab.sunnyclist

import com.here.android.mpa.mapping.Map
import com.here.android.mpa.routing.Route

object Globals {
    var debugMode = false
    var numberOfRoutes = 3
    var searchRadius = 350000
    var NavigationMode = false
    var bms_battery_soc = 0
    var current_speed = 0
    var cloud_intensity = 0
    var routeSelectionResult = false
    lateinit var activeRoute: Route
    lateinit var mainMap: Map
    var availableRoutes: MutableList<Route> = ArrayList()
    var routeConsumptions = arrayOf(0.0,0.0,0.0)
    var routeDistances = arrayOf(0.0,0.0,0.0)
    var selectedRoute = 0
    var batteryCapacity = 0.0
}