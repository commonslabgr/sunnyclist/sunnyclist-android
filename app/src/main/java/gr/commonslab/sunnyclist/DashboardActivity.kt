package gr.commonslab.sunnyclist

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.here.android.mpa.common.GeoCoordinate
import kotlinx.android.synthetic.main.activity_dashboard.*
import java.io.IOException
import java.util.*

class DashboardActivity : AppCompatActivity()  {
    private val REQUEST_CODE_SEARCHRESULT = 333
    //Bluetooth
    lateinit var readBuffer: ByteArray
    var readBufferPosition = 0
    private val batteryCapacity = 4.8F //Kwh

    private var currentBTdata = emptyList<String>()
    private var previousBTdata= emptyList<String>()
    private val btInterval:Long = 500 //How often to check for car data through BT and update screen

    @Volatile
    var stopWorker = false

    @SuppressLint("SetTextI18n")
    private fun updateDataOnScreen() {
        val speed = findViewById<TextView>(R.id.speed)
        val currentSolar = findViewById<TextView>(R.id.solarcurrent)
        val totalSolar = findViewById<TextView>(R.id.solartotal)
        val currentPdl1 = findViewById<TextView>(R.id.pedalcurrent1)
        val totalPdl1 = findViewById<TextView>(R.id.pedaltotal1)
        val currentPdl2 = findViewById<TextView>(R.id.pedalcurrent2)
        val totalPdl2 = findViewById<TextView>(R.id.pedaltotal2)
        val consumptionDiff = findViewById<TextView>(R.id.textConsumption)
        val distance = findViewById<TextView>(R.id.textdistance)
        val fullyCharged = findViewById<TextView>(R.id.textfullcharge)
        val parking = findViewById<ImageView>(R.id.imageparking)
        val warning = findViewById<ImageView>(R.id.imageWarning)
        val errorText = findViewById<TextView>(R.id.textError)
        val bmstext = findViewById<TextView>(R.id.bmstext)
        val redbox = findViewById<ImageView>(R.id.red_charging)
        val orangebox = findViewById<ImageView>(R.id.orange_charging)
        val yellowbox = findViewById<ImageView>(R.id.yellow_charging)
        val greenbox = findViewById<ImageView>(R.id.green_charging)
        val cyanbox = findViewById<ImageView>(R.id.cyan_charging)
        errorText.text = ""
        speed.text = ""
        currentSolar.text = ""
        totalSolar.text = ""
        currentPdl1.text = ""
        totalPdl1.text = ""
        currentPdl2.text = ""
        totalPdl2.text = ""
        consumptionDiff.text = ""
        distance.text = ""
        fullyCharged.text = ""
        parking.visibility = View.INVISIBLE
        redbox.visibility = View.INVISIBLE
        orangebox.visibility = View.INVISIBLE
        yellowbox.visibility = View.INVISIBLE
        greenbox.visibility = View.INVISIBLE
        cyanbox.visibility = View.INVISIBLE
        warning.visibility = View.VISIBLE
        var consumption : Float = 0.0f
        var bmsSoc = 0
        var bmsCurrentPower = 0.0f
        var parkingStatus = false
        var warningStatus = false
        var errorMsg = ""
        var speedData = "0"
        var curSolar = ""
        var totalSolarData: Double = 0.0
        var curPdl1 = ""
        var curPdl2 = ""
        var totalPdl1Data: Double = 0.0
        var totalPdl2Data: Double = 0.0

        Thread({
            while(true) {
                previousBTdata = currentBTdata
                if ((Bluetooth.initialized) && (Bluetooth.mmSocket.isConnected)) {
                    currentBTdata = readBTData()
                    if (currentBTdata.isNotEmpty()) {
                        speedData = currentBTdata[0]
                        curSolar = currentBTdata[2]
                        totalSolarData = currentBTdata[3].toDouble()
                        curPdl1 = currentBTdata[4]
                        totalPdl1Data = currentBTdata[5].toDouble()
                        curPdl2 = currentBTdata[6]
                        totalPdl2Data = currentBTdata[7].toDouble()
                        consumption = (currentBTdata[8].toFloat() * 3.6).toFloat() //turning Ws into kWh
                        bmsSoc = currentBTdata[9].toInt()
                        bmsCurrentPower = currentBTdata[11].toFloat()/1000 //Watts into kW
                        parkingStatus = currentBTdata[12].toBoolean()
                        warningStatus = currentBTdata[13].toBoolean()
                        errorMsg = currentBTdata[14]
                    }
                    runOnUiThread {
                        errorText.visibility = View.INVISIBLE
                        errorText.text = ""
                    }
                } else {
                    if (Bluetooth.findBT()) {
                        Bluetooth.openBT()
                    }
                    runOnUiThread {
                        errorText.visibility = View.VISIBLE
                        errorText.text = "Bluetooth connection not available."
                    }
                }
                if ((previousBTdata.isEmpty()) || (currentBTdata.isEmpty()))
                {
                    Thread.sleep(1000)
                    continue
                }
                val meanConsumption = 0.05 //kWh/km
                //SerialBT.printf("%d:%ld:%d:%d:%d:%d:%d:%d:%d:%d:%d:%s\n",speed,totaldistance,pwr_slrCurrentPower,pwr_slrTotalPower,pwr_pdl1CurrentPower,pwr_pdl1TotalPower,pwr_pdl2CurrentPower,pwr_pdl2TotalPower,bms_currentAvgPower,bms_soc,bms_totalConsumed,bms_currentPower,parkingStatus,warningStatus,errormsg);
                //add solar production
                val distanceLeft = (batteryCapacity * (bmsSoc/100))/meanConsumption
                val charged = ((1 - (bmsSoc/100.toFloat())) * batteryCapacity) / - bmsCurrentPower //h

                runOnUiThread {
                    speed.text = speedData //kmh
                    currentSolar.text = curSolar+" W"
                    totalSolar.text = String.format("%.2f Wh",(totalSolarData/3600))
                    currentPdl1.text = curPdl1+" W"
                    totalPdl1.text = String.format("%.2f Wh",(totalPdl1Data/3600))
                    currentPdl2.text = curPdl2+" W"
                    totalPdl2.text = String.format("%.2f Wh",(totalPdl2Data.toDouble()/3600))
                    consumptionDiff.text = String.format("%.2f kW", consumption)
                    distance.text = String.format("%.2f km",distanceLeft)
                    //If it is negative show nothing since the car is discharging
                    if (charged > 0) {
                        fullyCharged.text = "$charged hours"
                    } else {
                        fullyCharged.text = ""
                    }
                    if (parkingStatus) {
                        parking.visibility = View.VISIBLE
                    } else {
                        parking.visibility = View.INVISIBLE
                    }
                    if (warningStatus) {
                        warning.visibility = View.VISIBLE
                    } else {
                        warning.visibility = View.INVISIBLE
                    }
                    if (errorMsg.isNotEmpty()){
                        errorText.visibility = View.VISIBLE
                        errorText.text = errorMsg
                    } else {
                        errorText.visibility = View.INVISIBLE
                        errorText.text = ""
                    }
                    //BMS
                    bmstext.text = "$bmsSoc%"

                    if (bmsSoc > 5 ) {
                        //draw red
                        redbox.visibility = View.VISIBLE
                    }
                    if (bmsSoc > 20) {
                        //draw orange
                        orangebox.visibility = View.VISIBLE
                    }
                    if (bmsSoc > 40) {
                        //draw yellow
                        yellowbox.visibility = View.VISIBLE
                    }
                    if (bmsSoc > 60) {
                        //draw green
                        greenbox.visibility = View.VISIBLE
                    }
                    if (bmsSoc > 80) {
                        //draw cyan
                        cyanbox.visibility = View.VISIBLE
                    }
                }
                Thread.sleep(btInterval)
            }
        }, "DashboardActivity.RefreshValues").start()
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val param = intent.extras
        val longitude: Double = param?.getDouble("mapcenter_long")!!
        val latitude: Double = param.getDouble("mapcenter_lat")

        val center = GeoCoordinate(latitude, longitude)

        val bottomNavigation: BottomNavigationView = findViewById<BottomNavigationView>(R.id.navigationView)
        bottomNavigation.selectedItemId = R.id.navigation_dashboard
        val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    finish()
                }
                R.id.navigation_search -> {
                    val searchActivity = Intent(applicationContext, SearchActivity::class.java)
                    //Search by map center
                    searchActivity.putExtra("mapcenter_long", center.longitude)
                    searchActivity.putExtra("mapcenter_lat", center.latitude)
                    //Search by GPS location
                    startActivityForResult(searchActivity, REQUEST_CODE_SEARCHRESULT)
                }
                R.id.navigation_navigation -> {
                    //startNavigation()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_dashboard -> {
                    val dashboardActivity = Intent(applicationContext, DashboardActivity::class.java)
                    startActivity(dashboardActivity)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        updateDataOnScreen()
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //DEBUG for testing purposes
    fun dummyBTData():List<String> {
        val speed = Random(System.nanoTime()).nextInt(70-10+1)+10
        val totaldistance = 89
        val pwr_slrCurrentPower = 22
        val pwr_slrTotalPower = 23
        val pwr_pdl1CurrentPower = 1
        val pwr_pdl1TotalPower = 12
        val pwr_pdl2CurrentPower = 0
        val pwr_pdl2TotalPower = 123
        val bms_currentAvgPower = 6
        val bms_soc = 43
        val bms_totalConsumed = 12
        val bms_currentPower = 2
        val parkingStatus = 1
        val warningStatus = 0
        val errormsg = ""
        //DEBUG
        return "$speed:$totaldistance:$pwr_slrCurrentPower:$pwr_slrTotalPower:$pwr_pdl1CurrentPower:$pwr_pdl1TotalPower:$pwr_pdl2CurrentPower:$pwr_pdl2TotalPower:$bms_currentAvgPower:$bms_soc:$bms_totalConsumed:$bms_currentPower:$parkingStatus:$warningStatus:$errormsg".split(":")
    }

    private fun readBTData():List<String>  {
        val delimiter: Byte = 10 //This is the ASCII code for \n
        stopWorker = false
        readBufferPosition = 0
        readBuffer = ByteArray(1024)
        var data = ""
        try {
            val bytesAvailable = Bluetooth.mmInputStream.available()
            if (bytesAvailable > 0) {
                val packetBytes = ByteArray(bytesAvailable)
                Bluetooth.mmInputStream.read(packetBytes)
                for (i in 0 until bytesAvailable) {
                    val b = packetBytes[i]
                    if (b == delimiter) {
                        val encodedBytes = ByteArray(readBufferPosition)
                        System.arraycopy(readBuffer,0,encodedBytes,0,encodedBytes.size)
                        data += String(encodedBytes, Charsets.US_ASCII)
                        readBufferPosition = 0
                        return data.split(":")
                    } else {
                        readBuffer[readBufferPosition++] = b
                    }
                }
                return data.split(":")
            }
        } catch (ex: IOException) {
            stopWorker = true
            return emptyList<String>()
        }
        return emptyList<String>()
    }

}