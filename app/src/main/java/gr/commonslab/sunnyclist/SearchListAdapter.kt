import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.TextView
import com.here.android.mpa.search.DiscoveryResult
import gr.commonslab.sunnyclist.R
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class SearchListAdapter(private val context: Context, private var dataList: ArrayList<HashMap<String, String>>) : BaseAdapter() {

    var list = dataList
    private var tempdataList: ArrayList<java.util.HashMap<String, String>> = ArrayList(list)
    private val inflater: LayoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getCount(): Int {
        return dataList.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val discoveryResult = dataList[position]
        context
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.result_list_item, parent, false )
        }

        /*
         * Display title and vicinity information of each result.Please refer to HERE Android SDK
         * API doc for all supported APIs.
         */
        var tv = convertView!!.findViewById<View>(R.id.name) as TextView
        tv.text = Html.fromHtml(discoveryResult["title"])
        tv = convertView.findViewById<View>(R.id.vicinity) as TextView
        tv.text = Html.fromHtml(discoveryResult["vicinity"])
        return convertView
    }
}